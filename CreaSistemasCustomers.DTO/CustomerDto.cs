﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CreaSistemas.Customers.DTO
{
    public class CustomerDto
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "El campo Name es requerido")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "El campo IdentificationNumber es requerido")]
        [Display(Name = "IdentificationNumber")]
        public string IdentificationNumber { get; set; }

        public string Address { get; set; }
        public string Phone { get; set; }

        public bool IsActive { get; set; }
    }
}
