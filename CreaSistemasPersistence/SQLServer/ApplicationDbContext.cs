﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using CreaSistemas.Domain.Entities;

namespace CreaSistemas.Persistence.SQLServer
{
    public class ApplicationDbContext : DbContext
    {
        #region Colecciones BD
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Sale> Sales { get; set; }
        public DbSet<ItemSale> ItemsSale { get; set; }
        #endregion

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
