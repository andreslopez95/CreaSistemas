﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CreaSistemas.Persistence.SQLServer
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IUnitOfWork _unitOfWork;

        public Repository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        #region Transactional

        public async Task<IDbContextTransaction> BeginTransactionAsync()
        {
            return await _unitOfWork.Context.Database.BeginTransactionAsync();
        }

        public void CommitTransaction()
        {
            _unitOfWork.Context.Database.CommitTransaction();
        }

        public void RollbackTransaction()
        {
            _unitOfWork.Context.Database.RollbackTransaction();
        }
        #endregion

        public async Task<IQueryable<T>> GetAllAsync(
            Expression<Func<T, bool>> predicate = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null)
        {
            try
            {
                IQueryable<T> query = _unitOfWork.Context.Set<T>().AsQueryable();
                query = PrepareQuery(query, predicate, include, orderBy); ;
                return await Task.Run(() => query);
            }
            catch (Exception ex)
            {
                throw new Exception("Ha ocurrido un error listando las entidades", ex);
            }
        }

        public virtual async Task<T> FirstOrDefaultAsync(
            Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null
        )
        {
            try
            {
                IQueryable<T> query = _unitOfWork.Context.Set<T>().AsQueryable().AsNoTracking();
                query = PrepareQuery(query, predicate, include, orderBy);
                return await query.FirstOrDefaultAsync();

            }
            catch (Exception ex)
            {
                throw new Exception("Ha ocurrido un error buscando la entidad", ex);
            }
        }

        public async Task<T> CreateAsync(T entity)
        {
            try
            {
                await _unitOfWork.Context.Set<T>().AddAsync(entity);
                await SaveAllAsync();
                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception("Ha ocurrido un error insertando la entidad " + entity.GetType().Name, ex);
            }
        }

        public async Task<bool> UpdateAsync(T entity)
        {
            try
            {
                _unitOfWork.Context.Set<T>().Update(entity);
                await _unitOfWork.Context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw new Exception("Ha ocurrido un error actualizando la entidad", ex);
            }
        }

        #region Metodos internos        
        protected IQueryable<T> PrepareQuery(
            IQueryable<T> query,
            Expression<Func<T, bool>> predicate = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null
        )
        {
            if (include != null)
            {
                query = include(query);
            }

            if (predicate != null)
            {
                query = query.Where(predicate);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            return query;
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _unitOfWork.Context.SaveChangesAsync() > 0;
        }
        #endregion
    }
}
