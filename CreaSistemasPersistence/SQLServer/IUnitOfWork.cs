﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreaSistemas.Persistence.SQLServer
{
    public interface IUnitOfWork : IDisposable
    {
        ApplicationDbContext Context { get; }
        void Commit();
    }
}
