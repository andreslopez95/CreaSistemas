﻿using CreaSistemas.Domain.Entities;
using CreaSistemas.Persistence.SQLServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreaSistemas.Persistence.Seeds
{
    public class MainSeeder
    {
        private readonly ApplicationDbContext _context;

        public MainSeeder(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task SeedAsync()
        {
            await _context.Database.EnsureCreatedAsync();

            if (_context.Customers.Count() <= 0)
            {
                await CustomersAsync();
            }

            if (_context.Products.Count() <= 0)
            {
                await ProductsAsync();
            }

            if (_context.Sales.Count() <= 0)
            {
                await SalesAsync();
            }

            if (_context.ItemsSale.Count() <= 0)
            {
                await ItemsSaleAsync();
            }
        }

        #region Métodos
        private async Task CustomersAsync()
        {
            var customers = new List<Customer>()
            {
                new Customer { Id = Guid.Parse("D92FFCF6-FD79-4F28-8099-D0F70643468B"), Name = "Carolina Perez", IdentificationNumber = "20948563", Address = "", Phone = "", IsActive = true},
                new Customer { Id = Guid.Parse("262518C0-44EF-4CEE-A0AB-E509C2C48833"), Name = "Andres Lopez", IdentificationNumber = "10715344", Address = "", Phone = "", IsActive = true},
                new Customer { Id = Guid.Parse("3A68BC91-E941-4F53-B54B-C26161AFDEED"), Name = "Juan Garzon", IdentificationNumber = "3236210", Address = "", Phone = "", IsActive = true},
            };

            await _context.AddRangeAsync(customers);
            await _context.SaveChangesAsync();
        }

        private async Task ProductsAsync()
        {
            var products = new List<Product>()
            {
                new Product { Id = Guid.Parse("6F11D3A8-9D65-4F22-957D-9A50AF8AE7EE"), Code = "M001", Name = "Monitor Curvo", UnitPrice = 600000, Description = "", IsActive = true},
                new Product { Id = Guid.Parse("5CA3DA2E-DB79-488B-9024-F87BC084B7B6"), Code = "T001", Name = "Teclado", UnitPrice = 50000, Description = "", IsActive = true},
                new Product { Id = Guid.Parse("465B365E-E886-4CDC-8FB4-344D5717F151"), Code = "M001", Name = "Mouse", UnitPrice = 25000, Description = "", IsActive = true},
            };

            await _context.AddRangeAsync(products);
            await _context.SaveChangesAsync();
        }

        private async Task SalesAsync()
        {
            var sales = new List<Sale>()
            {
                new Sale { Id = Guid.Parse("B6EEC43B-C18D-4EDD-8BD3-81FCACA21A90"), CustomerId = Guid.Parse("D92FFCF6-FD79-4F28-8099-D0F70643468B"), Date = DateTime.UtcNow, Total = 600000, IsCancelled = false},
                new Sale { Id = Guid.Parse("1A9F79E2-C7DC-41AE-B719-36CA07F8F5CE"), CustomerId = Guid.Parse("262518C0-44EF-4CEE-A0AB-E509C2C48833"), Date = DateTime.UtcNow, Total = 75000, IsCancelled = false},                
            };

            await _context.AddRangeAsync(sales);
            await _context.SaveChangesAsync();
        }

        private async Task ItemsSaleAsync()
        {
            var itemsSale = new List<ItemSale>()
            {
                new ItemSale { Id = Guid.Parse("0BEF4AE6-4B94-4729-98E0-5E679E35BDD2"), SaleId = Guid.Parse("B6EEC43B-C18D-4EDD-8BD3-81FCACA21A90"), ProductId = Guid.Parse("6F11D3A8-9D65-4F22-957D-9A50AF8AE7EE"), Quantity = 1, SubTotal= 60000},
                new ItemSale { Id = Guid.Parse("77D414E3-846D-4B30-9A76-54DD0C1C911A"), SaleId = Guid.Parse("1A9F79E2-C7DC-41AE-B719-36CA07F8F5CE"), ProductId = Guid.Parse("5CA3DA2E-DB79-488B-9024-F87BC084B7B6"), Quantity = 1, SubTotal= 50000},
                new ItemSale { Id = Guid.Parse("68166119-1D8F-4AC2-8212-D9E61D0F1B2A"), SaleId = Guid.Parse("1A9F79E2-C7DC-41AE-B719-36CA07F8F5CE"), ProductId = Guid.Parse("465B365E-E886-4CDC-8FB4-344D5717F151"), Quantity = 1, SubTotal= 25000},
            };

            await _context.AddRangeAsync(itemsSale);
            await _context.SaveChangesAsync();
        }
        #endregion
    }
}
