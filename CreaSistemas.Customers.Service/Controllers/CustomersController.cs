﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreaSistemas.Customers.Business;
using CreaSistemas.Customers.Business.Utility;
using CreaSistemas.Customers.DTO;
using CreaSistemas.Domain.Entities;
using CreaSistemas.Persistence.SQLServer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CreaSistemas.Customers.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerBusiness _oRepository;                
        private readonly ILogger<CustomersController> _logger;

        public CustomersController(ICustomerBusiness oRepository, ILogger<CustomersController> logger)
        {
            _oRepository = oRepository;            
            _logger = logger;
        }

        /// <summary>
        /// Busca clientes por nombre o número de identificación, si no se envia filtro se listan todos los clientes. 
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<CustomerDto>> GetCustomers(string filter)
        {
            _logger.LogInformation("Called GetCustomers");
            try
            {
                return await _oRepository.GetCustomers(filter);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in GetCustomers");
                return (IQueryable<CustomerDto>)NotFound();
            }
        }        
        
        /// <summary>
        /// Crea un cliente 
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Customer>> CreateCustomer(CustomerDto Dto)
        {
            _logger.LogInformation("Called CreateCustomer");
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }
                
                await _oRepository.CreateCustomer(Dto);                

                return CreatedAtAction(nameof(GetCustomers), new { id = Dto.Id }, Dto);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in CreateCustomer");
                return NotFound();
            }
        }

        /// <summary>
        /// Actualiza un cliente 
        /// </summary>
        [HttpPut]
        public async Task<IActionResult> ModifyCustomer(CustomerDto Dto)
        {
            _logger.LogInformation("Called ModifyCustomer");
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                await _oRepository.ModifyCustomer(Dto);                

                return CreatedAtAction(nameof(GetCustomers), new { id = Dto.Id }, Dto);                
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in ModifyCustomer");
                return NotFound();
            }
        }
        
        /// <summary>
        /// Desactiva un cliente por número de identificación
        /// </summary>
        [HttpPut("{identificationNumber}")]
        public async Task<IActionResult> DisableCustomer(string identificationNumber)
        {
            _logger.LogInformation("Called DisableCustomer");
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }
                                
                await _oRepository.DisableCustomer(identificationNumber);                

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in DisableCustomer");
                return NotFound();
            }
        }
    }
}
