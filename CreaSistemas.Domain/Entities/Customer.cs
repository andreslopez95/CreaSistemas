﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreaSistemas.Domain.Entities
{
    [Table("CUSTOMERS")]
    public class Customer
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string IdentificationNumber { get; set; }

        public string Address { get; set; }
        public string Phone { get; set; }

        public bool IsActive { get; set; }       
    }
}
