﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreaSistemas.Domain.Entities
{
    [Table("SALES")]
    public class Sale
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid CustomerId { get; set; }        

        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public List<ItemSale> ItemsSale { get; set; }

        [Required]
        public decimal Total { get; set; }

        public bool IsCancelled { get; set; }
    }
}
