﻿using CreaSistemas.Customers.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreaSistemas.Customers.Business
{
    public interface ICustomerBusiness
    {
        Task<IEnumerable<CustomerDto>> GetCustomers(string filter);
        Task<bool> CreateCustomer(CustomerDto dto);
        Task<bool> ModifyCustomer(CustomerDto dto);
        Task<bool> DisableCustomer(string identificationNumber);
    }
}
