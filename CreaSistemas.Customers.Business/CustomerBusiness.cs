﻿using CreaSistemas.Customers.Business.Utility;
using CreaSistemas.Customers.DTO;
using CreaSistemas.Domain.Entities;
using CreaSistemas.Persistence.SQLServer;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreaSistemas.Customers.Business
{
    public class CustomerBusiness : ICustomerBusiness
    {
        private readonly IRepository<Customer> _repository;        

        public CustomerBusiness(IRepository<Customer> repository)
        {
            _repository = repository;                     
        }        

        public async Task<IEnumerable<CustomerDto>> GetCustomers(string filter)
        {
            var list = string.IsNullOrEmpty(filter) ? await _repository.GetAllAsync() : await _repository.GetAllAsync(predicate: source => source.Name.Contains(filter) || source.IdentificationNumber.Contains(filter));
            return AutoMapperConfig.GetMapper<Customer, CustomerDto>().Map<List<CustomerDto>>(list);
        }

        public async Task<bool> CreateCustomer(CustomerDto Dto)
        {
            await _repository.BeginTransactionAsync();
                try
                {
                    var oMapper = AutoMapperConfig.GetMapper<Customer, CustomerDto>().Map<Customer>(Dto);
                    await _repository.CreateAsync(oMapper);
                    _repository.CommitTransaction();
                    
                    return true;
                }
                catch (Exception ex)
                {
                    _repository.RollbackTransaction();
                    return false;
                    throw new Exception("Ha ocurrido un error creando la entidad", ex);
                }            
        }

        public async Task<bool> ModifyCustomer(CustomerDto Dto)
        {
            await _repository.BeginTransactionAsync();
            try
            {
                var vConsulta = await _repository.FirstOrDefaultAsync(predicate: source => source.Id.Equals(Dto.Id));
                if (vConsulta == null)
                {
                    return false;
                }

                var oMapper = AutoMapperConfig.GetMapper<Customer, CustomerDto>().Map(Dto, vConsulta);
                await _repository.UpdateAsync(vConsulta);
                _repository.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                _repository.RollbackTransaction();
                return false;
                throw new Exception("Ha ocurrido un error creando la entidad", ex);
            }
        }

        public async Task<bool> DisableCustomer(string identificationNumber)
        {
            await _repository.BeginTransactionAsync();
            try
            {
                var vConsulta = await _repository.FirstOrDefaultAsync(predicate: source => source.IdentificationNumber.Equals(identificationNumber));
                if (vConsulta == null)
                {
                    return false;
                }

                var oMapper = AutoMapperConfig.GetMapper<Customer, CustomerDto>().Map<Customer>(vConsulta);
                oMapper.IsActive = false;                
                await _repository.UpdateAsync(vConsulta);
                _repository.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                _repository.RollbackTransaction();
                return false;
                throw new Exception("Ha ocurrido un error creando la entidad", ex);
            }
        }
    }
}
