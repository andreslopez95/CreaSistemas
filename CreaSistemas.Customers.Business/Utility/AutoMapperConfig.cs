﻿using AutoMapper;
using CreaSistemas.Customers.DTO;
using CreaSistemas.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreaSistemas.Customers.Business.Utility
{
    public class AutoMapperConfig
    {
        public static IMapper GetMapper<T1, T2>()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<T1, T2>().ReverseMap();
                cfg.CreateMap<Guid, string>().ConvertUsing(o => o.ToString());
                cfg.CreateMap<Customer, CustomerDto>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }
    }
}
