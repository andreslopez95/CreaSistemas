﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreaSistemas.Domain.Entities;
using CreaSistemas.Products.Business;
using CreaSistemas.Products.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CreaSistemas.Products.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductBusiness _oRepository;
        private readonly ILogger<ProductsController> _logger;

        public ProductsController(IProductBusiness oRepository, ILogger<ProductsController> logger)
        {
            _oRepository = oRepository;
            _logger = logger;
        }

        /// <summary>
        /// Busca productos por nombre o código, si no se envia filtro se listan todos los productos. 
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<ProductDto>> GetProducts(string filter)
        {
            _logger.LogInformation("Called GetProducts");
            try
            {
                return await _oRepository.GetProducts(filter);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in GetProducts");
                return (IQueryable<ProductDto>)NotFound();
            }
        }

        /// <summary>
        /// Crea un producto 
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Product>> CreateProduct(ProductDto Dto)
        {
            _logger.LogInformation("Called CreateProduct");
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                await _oRepository.CreateProduct(Dto);

                return CreatedAtAction(nameof(GetProducts), new { id = Dto.Id }, Dto);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in CreateProduct");
                return NotFound();
            }
        }

        /// <summary>
        /// Actualiza un producto 
        /// </summary>
        [HttpPut]
        public async Task<IActionResult> ModifyProduct(ProductDto Dto)
        {
            _logger.LogInformation("Called ModifyProduct");
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                await _oRepository.ModifyProduct(Dto);

                return CreatedAtAction(nameof(GetProducts), new { id = Dto.Id }, Dto);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in ModifyProduct");
                return NotFound();
            }
        }

        /// <summary>
        /// Desactiva un producto por código
        /// </summary>
        [HttpPut("{code}")]
        public async Task<IActionResult> DisableProduct(string code)
        {
            _logger.LogInformation("Called DisableProduct");
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                await _oRepository.DisableProduct(code);

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in DisableProduct");
                return NotFound();
            }
        }
    }
}
