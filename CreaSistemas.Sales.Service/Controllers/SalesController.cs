﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreaSistemas.Domain.Entities;
using CreaSistemas.Sales.Business;
using CreaSistemas.Sales.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CreaSistemas.Sales.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private readonly ISaleBusiness _oRepository;
        private readonly ILogger<SalesController> _logger;

        public SalesController(ISaleBusiness oRepository, ILogger<SalesController> logger)
        {
            _oRepository = oRepository;
            _logger = logger;
        }

        /// <summary>
        /// Busca ventas por nombre o número de identificación de un cliente, si no se envia filtro se listan todos las ventas. 
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<SaleDto>> GetSales(string filter)
        {
            _logger.LogInformation("Called GetSales");
            try
            {
                return await _oRepository.GetSales(filter);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in GetSales");
                return (IQueryable<SaleDto>)NotFound();
            }
        }

        /// <summary>
        /// Crea un venta 
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<Sale>> CreateSale(SaleCreateDto Dto)
        {
            _logger.LogInformation("Called CreateSale");
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                await _oRepository.CreateSale(Dto);

                return CreatedAtAction(nameof(GetSales), new { Dto.Date }, Dto);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in CreateSale");
                return NotFound();
            }
        }

        /// <summary>
        /// Actualiza un venta 
        /// </summary>
        [HttpPut]
        public async Task<IActionResult> ModifySale(SaleDto Dto)
        {
            _logger.LogInformation("Called ModifySale");
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                await _oRepository.ModifySale(Dto);

                return CreatedAtAction(nameof(GetSales), new { id = Dto.Id }, Dto);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in ModifySale");
                return NotFound();
            }
        }

        /// <summary>
        /// Anula una venta por el id
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> DisableSale(Guid id)
        {
            _logger.LogInformation("Called DisableSale");
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                await _oRepository.CancelSale(id);

                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error in DisableSale");
                return NotFound();
            }
        }
    }
}
