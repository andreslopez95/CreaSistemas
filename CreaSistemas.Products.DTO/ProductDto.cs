﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CreaSistemas.Products.DTO
{
    public class ProductDto
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "El campo Code es requerido")]
        [Display(Name = "Code")]
        public string Code { get; set; }

        [Required(ErrorMessage = "El campo Name es requerido")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        
        [Column(TypeName = "decimal(18,2)")]
        public decimal UnitPrice { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }
    }
}
