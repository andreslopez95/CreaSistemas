﻿using CreaSistemas.Products.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreaSistemas.Sales.DTO
{
    public class ItemSaleDto
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "El campo SaleId es requerido")]
        [Display(Name = "SaleId")]
        public Guid SaleId { get; set; }

        [Required(ErrorMessage = "El campo ProductId es requerido")]
        [Display(Name = "ProductId")]
        public Guid ProductId { get; set; }
        
        public ProductDto Product { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public int Quantity { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal SubTotal { get; set; }
    }
}
