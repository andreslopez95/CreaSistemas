﻿using CreaSistemas.Customers.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CreaSistemas.Sales.DTO
{
    public class SaleDto
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "El campo CustomerId es requerido")]
        [Display(Name = "CustomerId")]
        public Guid CustomerId { get; set; }
        public CustomerDto Customer { get; set; }

        [Required(ErrorMessage = "El campo Date es requerido")]
        [Display(Name = "Date")]
        public DateTime Date { get; set; }
        
        public List<ItemSaleDto> ItemsSale { get; set; }

        [Column(TypeName = "decimal(18,2)")]
        public decimal Total { get; set; }

        public bool IsCancelled { get; set; }
    }
}
