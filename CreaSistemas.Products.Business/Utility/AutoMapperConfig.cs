﻿using AutoMapper;
using CreaSistemas.Domain.Entities;
using CreaSistemas.Products.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreaSistemas.Products.Business.Utility
{
    public class AutoMapperConfig
    {
        public static IMapper GetMapper<T1, T2>()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<T1, T2>().ReverseMap();
                cfg.CreateMap<Guid, string>().ConvertUsing(o => o.ToString());
                cfg.CreateMap<Product, ProductDto>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }
    }
}
