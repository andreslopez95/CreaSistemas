﻿using CreaSistemas.Products.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CreaSistemas.Products.Business
{
    public interface IProductBusiness
    {
        Task<IEnumerable<ProductDto>> GetProducts(string filter);
        Task<bool> CreateProduct(ProductDto dto);
        Task<bool> ModifyProduct(ProductDto dto);
        Task<bool> DisableProduct(string code);
    }
}
