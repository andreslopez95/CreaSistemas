﻿using CreaSistemas.Domain.Entities;
using CreaSistemas.Persistence.SQLServer;
using CreaSistemas.Products.Business.Utility;
using CreaSistemas.Products.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreaSistemas.Products.Business
{
    public class ProductBusiness : IProductBusiness
    {
        private readonly IRepository<Product> _repository;

        public ProductBusiness(IRepository<Product> repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<ProductDto>> GetProducts(string filter)
        {
            var list = string.IsNullOrEmpty(filter) ? await _repository.GetAllAsync() : await _repository.GetAllAsync(predicate: source => source.Name.Contains(filter) || source.Code.Contains(filter));
            return AutoMapperConfig.GetMapper<Product, ProductDto>().Map<List<ProductDto>>(list);
        }

        public async Task<bool> CreateProduct(ProductDto Dto)
        {
            await _repository.BeginTransactionAsync();
            try
            {
                var oMapper = AutoMapperConfig.GetMapper<Product, ProductDto>().Map<Product>(Dto);
                await _repository.CreateAsync(oMapper);
                _repository.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                _repository.RollbackTransaction();
                return false;
                throw new Exception("Ha ocurrido un error creando la entidad", ex);
            }
        }

        public async Task<bool> ModifyProduct(ProductDto Dto)
        {
            await _repository.BeginTransactionAsync();
            try
            {
                var vConsulta = await _repository.FirstOrDefaultAsync(predicate: source => source.Id.Equals(Dto.Id));
                if (vConsulta == null)
                {
                    return false;
                }

                var oMapper = AutoMapperConfig.GetMapper<Product, ProductDto>().Map(Dto, vConsulta);
                await _repository.UpdateAsync(vConsulta);
                _repository.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                _repository.RollbackTransaction();
                return false;
                throw new Exception("Ha ocurrido un error creando la entidad", ex);
            }
        }

        public async Task<bool> DisableProduct(string identificationNumber)
        {
            await _repository.BeginTransactionAsync();
            try
            {
                var vConsulta = await _repository.FirstOrDefaultAsync(predicate: source => source.Code.Equals(identificationNumber));
                if (vConsulta == null)
                {
                    return false;
                }

                var oMapper = AutoMapperConfig.GetMapper<Product, ProductDto>().Map<Product>(vConsulta);
                oMapper.IsActive = false;
                await _repository.UpdateAsync(vConsulta);
                _repository.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                _repository.RollbackTransaction();
                return false;
                throw new Exception("Ha ocurrido un error creando la entidad", ex);
            }
        }
    }
}
