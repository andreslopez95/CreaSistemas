﻿using AutoMapper;
using CreaSistemas.Customers.DTO;
using CreaSistemas.Domain.Entities;
using CreaSistemas.Products.DTO;
using CreaSistemas.Sales.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CreaSistemas.Sales.Business.Utility
{
    public class AutoMapperConfig
    {
        public static IMapper GetMapper<T1, T2>()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<T1, T2>().ReverseMap();
                cfg.CreateMap<Guid, string>().ConvertUsing(o => o.ToString());
                cfg.CreateMap<Sale, SaleDto>();
                cfg.CreateMap<Sale, SaleCreateDto>();
                cfg.CreateMap<ItemSale, ItemSaleDto>();
                cfg.CreateMap<Customer, CustomerDto>();
                cfg.CreateMap<Product, ProductDto>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }
    }
}
