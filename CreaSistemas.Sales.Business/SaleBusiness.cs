﻿using CreaSistemas.Domain.Entities;
using CreaSistemas.Persistence.SQLServer;
using CreaSistemas.Sales.Business.Utility;
using CreaSistemas.Sales.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CreaSistemas.Sales.Business
{
    public class SaleBusiness : ISaleBusiness
    {
        private readonly IRepository<Sale> _repository;

        public SaleBusiness(IRepository<Sale> repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<SaleDto>> GetSales(string filter)
        {
            var list = string.IsNullOrEmpty(filter) ? await _repository.GetAllAsync(include: source => source.Include(x => x.Customer).Include(x => x.ItemsSale).ThenInclude(x => x.Product))
                                                                                        : await _repository.GetAllAsync(include: source => source .Include(x => x.Customer).Include(x => x.ItemsSale).ThenInclude(x => x.Product),
                                                                                                                            predicate: source => source.Customer.Name.Contains(filter) || source.Customer.IdentificationNumber.Contains(filter));
            return AutoMapperConfig.GetMapper<Sale, SaleDto>().Map<List<SaleDto>>(list);
        }

        public async Task<bool> CreateSale(SaleCreateDto Dto)
        {
            await _repository.BeginTransactionAsync();
            try
            {
                var oMapper = AutoMapperConfig.GetMapper<Sale, SaleCreateDto>().Map<Sale>(Dto);
                await _repository.CreateAsync(oMapper);
                _repository.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                _repository.RollbackTransaction();
                return false;
                throw new Exception("Ha ocurrido un error creando la entidad", ex);
            }
        }

        public async Task<bool> ModifySale(SaleDto Dto)
        {
            await _repository.BeginTransactionAsync();
            try
            {
                var vConsulta = await _repository.FirstOrDefaultAsync(predicate: source => source.Id.Equals(Dto.Id));
                if (vConsulta == null)
                {
                    return false;
                }

                var oMapper = AutoMapperConfig.GetMapper<Sale, SaleDto>().Map(Dto, vConsulta);
                await _repository.UpdateAsync(vConsulta);
                _repository.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                _repository.RollbackTransaction();
                return false;
                throw new Exception("Ha ocurrido un error creando la entidad", ex);
            }
        }

        public async Task<bool> CancelSale(Guid id)
        {
            await _repository.BeginTransactionAsync();
            try
            {
                var vConsulta = await _repository.FirstOrDefaultAsync(predicate: source => source.Id.Equals(id));
                if (vConsulta == null)
                {
                    return false;
                }

                var oMapper = AutoMapperConfig.GetMapper<Sale, SaleDto>().Map<Sale>(vConsulta);
                oMapper.IsCancelled = true;
                await _repository.UpdateAsync(vConsulta);
                _repository.CommitTransaction();

                return true;
            }
            catch (Exception ex)
            {
                _repository.RollbackTransaction();
                return false;
                throw new Exception("Ha ocurrido un error creando la entidad", ex);
            }
        }
    }
}
