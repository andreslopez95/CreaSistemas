﻿using CreaSistemas.Sales.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CreaSistemas.Sales.Business
{
    public interface ISaleBusiness
    {
        Task<IEnumerable<SaleDto>> GetSales(string filter);
        Task<bool> CreateSale(SaleCreateDto dto);
        Task<bool> ModifySale(SaleDto dto);
        Task<bool> CancelSale(Guid id);
    }
}
